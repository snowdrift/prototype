#!/bin/bash
# Description: a script to do a rough hamlet-to-html conversion for simple
# pages. Supports up to 6 levels of nested tags. Dependencies: bc.



_self="$(readlink -f $0)"
tmp="${1%.*}.tmp"
input="$1"
output="${1%.*}.html"

# Output file to the same location as input file
if [ $(dirname $output) == "." ]; then
  input="$(dirname $_self)/$input"
  output="$(dirname $_self)/$output"
  tmp="$(dirname $_self)/$tmp"
fi

# Check input file exists
if ! [ -f "$input" ]; then
  echo "Error: cannot find input file."; exit 1
fi

echo "Closing open tags ..."
rm -rf $tmp
OIFS="$IFS"
while IFS="" read line; do
  # otags[0] holds the current tag
  otags[0]=$(echo "$line" | cut -f2 -d"<" | cut -f1 -d">" | cut -d" " -f1)
  # Remove escape character "#" from inner html
  case "$pv_line" in
    *\>*\#)
      inner_html=$(echo "$pv_line" | cut -f2 -d">")
      clean_inner=$(echo "$inner_html" | sed "s|#||")
      sed -i "s|$inner_html|$clean_inner|" $tmp
  esac
  case "$line" in
    *br\ \/\>|*hr\ \/\>) # Empty element
      clean_line=$(echo "$line" | sed "s|<${otags[0]} />|<${otags[0]}>|")
      echo "$clean_line" >> $tmp
      ;;
    \ \ \ \ \ \ \ \ \ \ \<*) # L6 tag
      if [ ! "${otags[6]}" == "" ]; then
        echo -e "          </${otags[6]}>\n$line" >> $tmp
        otags[6]="${otags[0]}"
      else
        otags[6]="${otags[0]}"
        inner_html=$(echo "$line" | cut -f2 -d">")
        full_tag="<"$(echo "$line" | cut -f2 -d"<" | cut -f1 -d">")">"
        if [[ "          $full_tag" =~ "$line" ]]; then
          echo -e "          $full_tag$inner_html" >> $tmp
        else
          # If text is on the same line as the tag, close the tag
          # at the end of the line
          closed_line="          $full_tag$inner_html</${otags[6]}>"
          echo -e "$closed_line" >> $tmp
          otags[6]=""
        fi
      fi
      ;;
    \ \ \ \ \ \ \ \ \<*) # L5 tag
      if [ ! "${otags[6]}" == "" ]; then
        echo -e "          </${otags[6]}>\n        </${otags[5]}>" >> $tmp
        echo -e "$line" >> $tmp
        otags[6]=""; otags[5]="${otags[0]}"
      elif [ ! "${otags[5]}" == "" ]; then
        echo -e "        </${otags[5]}>\n$line" >> $tmp
        otags[5]="${otags[0]}"
      else
        otags[5]="${otags[0]}"
        inner_html=$(echo "$line" | cut -f2 -d">")
        full_tag="<"$(echo "$line" | cut -f2 -d"<" | cut -f1 -d">")">"
        if [[ "        $full_tag" =~ "$line" ]]; then
          echo -e "        $full_tag$inner_html" >> $tmp
        else
          # If text is on the same line as the tag, close the tag
          # at the end of the line
          closed_line="        $full_tag$inner_html</${otags[5]}>"
          echo -e "$closed_line" >> $tmp
          otags[5]=""
        fi
      fi
      ;;
    \ \ \ \ \ \ \<*) # L4 tag
      if [ ! "${otags[6]}" == "" ]; then
        echo -e "          </${otags[6]}>\n        </${otags[5]}>" >> $tmp
        echo -e "      </${otags[4]}>\n$line" >> $tmp
        otags[6]=""; otags[5]=""; otags[4]="${otags[0]}"
      elif [ ! "${otags[5]}" == "" ]; then
        echo -e "        </${otags[5]}>\n      </${otags[4]}>\n$line" >> $tmp
        otags[5]=""; otags[4]="${otags[0]}"
      elif [ ! "${otags[4]}" == "" ]; then
        echo -e "      </${otags[4]}>\n$line" >> $tmp
        otags[4]="${otags[0]}"
      else
        otags[4]="${otags[0]}"
        inner_html=$(echo "$line" | cut -f2 -d">")
        full_tag="<"$(echo "$line" | cut -f2 -d"<" | cut -f1 -d">")">"
        if [[ "      $full_tag" =~ "$line" ]]; then
          echo -e "      $full_tag$inner_html" >> $tmp
        else
          # If text is on the same line as the tag, close the tag
          # at the end of the line
          closed_line="      $full_tag$inner_html</${otags[4]}>"
          echo -e "$closed_line" >> $tmp
          otags[4]=""
        fi
      fi
      ;;
    \ \ \ \ \<*) # L3 tag
      if [ ! "${otags[6]}" == "" ]; then
        echo -e "          </${otags[6]}>\n        </${otags[5]}>" >> $tmp
        echo -e "      </${otags[4]}>\n    </${otags[3]}>\n$line" >> $tmp
        otags[6]=""; otags[5]=""; otags[4]=""; otags[3]="${otags[0]}"
      elif [ ! "${otags[5]}" == "" ]; then
        echo -e "        </${otags[5]}>\n      </${otags[4]}>" >> $tmp
        echo -e "    </${otags[3]}>\n$line" >> $tmp
        otags[5]=""; otags[4]=""; otags[3]="${otags[0]}"
      elif [ ! "${otags[4]}" == "" ]; then
        echo -e "      </${otags[4]}>\n    </${otags[3]}>" >> $tmp
        echo -e "$line" >> $tmp
        otags[4]=""; otags[3]="${otags[0]}"
      elif [ ! "${otags[3]}" == "" ]; then
        echo -e "    </${otags[3]}>\n$line" >> $tmp
        otags[3]="${otags[0]}"
      else
        otags[3]="${otags[0]}"
        inner_html=$(echo "$line" | cut -f2 -d">")
        full_tag="<"$(echo "$line" | cut -f2 -d"<" | cut -f1 -d">")">"
        if [[ "    $full_tag" =~ "$line" ]]; then
          echo -e "    $full_tag$inner_html" >> $tmp
        else
          # If text is on the same line as the tag, close the tag
          # at the end of the line
          closed_line="    $full_tag$inner_html</${otags[3]}>"
          echo -e "$closed_line" >> $tmp
          otags[3]=""
        fi
      fi
      ;;
    \ \ \<*) # L2 tag
      if [ ! "${otags[6]}" == "" ]; then
        echo -e "          </${otags[6]}>\n        </${otags[5]}>" >> $tmp
        echo -e "      </${otags[4]}>\n    </${otags[3]}>" >> $tmp
        echo -e "  </${otags[2]}>\n$line" >> $tmp
        otags[6]=""; otags[5]=""; otags[4]=""; otags[3]=""
        otags[2]="${otags[0]}"
      elif [ ! "${otags[5]}" == "" ]; then
        echo -e "        </${otags[5]}>\n      </${otags[4]}>" >> $tmp
        echo -e "    </${otags[3]}>\n  </${otags[2]}>\n$line" >> $tmp
        otags[5]=""; otags[4]=""; otags[3]=""; otags[2]="${otags[0]}"
      elif [ ! "${otags[4]}" == "" ]; then
        echo -e "      </${otags[4]}>\n    </${otags[3]}>" >> $tmp
        echo -e "  </${otags[2]}>\n$line" >> $tmp
        otags[4]=""; otags[3]=""; otags[2]="${otags[0]}"
      elif [ ! "${otags[3]}" == "" ]; then
        echo -e "    </${otags[3]}>\n  </${otags[2]}>\n$line" >> $tmp
        otags[3]=""; otags[2]="${otags[0]}"
      elif [ ! "${otags[2]}" == "" ]; then
        echo -e "  </${otags[2]}>\n$line" >> $tmp
        otags[2]="${otags[0]}"
      else
        otags[2]="${otags[0]}"
        inner_html=$(echo "$line" | cut -f2 -d">")
        full_tag="<"$(echo "$line" | cut -f2 -d"<" | cut -f1 -d">")">"
        if [[ "  $full_tag" =~ "$line" ]]; then
          echo -e "  $full_tag$inner_html" >> $tmp
        else
          # If text is on the same line as the tag, close the tag
          # at the end of the line
          closed_line="  $full_tag$inner_html</${otags[2]}>"
          echo -e "$closed_line" >> $tmp
          otags[2]=""
        fi
      fi
      ;;
    \<*) # L1 tag
      if [ ! "${otags[6]}" == "" ]; then
        echo -e "          </${otags[6]}>\n        </${otags[5]}>" >> $tmp
        echo -e "      </${otags[4]}>\n    </${otags[3]}>" >> $tmp
        echo -e "  </${otags[2]}>\n</${otags[1]}>\n$line" >> $tmp
        otags[6]=""; otags[5]=""; otags[4]=""; otags[3]=""; otags[2]=""
        otags[1]="${otags[0]}"
      elif [ ! "${otags[5]}" == "" ]; then
        echo -e "        </${otags[5]}>\n      </${otags[4]}>" >> $tmp
        echo -e "    </${otags[3]}>\n  </${otags[2]}>\n</${otags[1]}>" >> $tmp
        echo -e "$line" >> $tmp
        otags[5]=""; otags[4]=""; otags[3]=""; otags[2]=""
        otags[1]="${otags[0]}"
      elif [ ! "${otags[4]}" == "" ]; then
        echo -e "      </${otags[4]}>\n    </${otags[3]}>" >> $tmp
        echo -e "  </${otags[2]}>\n</${otags[1]}>\n$line" >> $tmp
        otags[4]=""; otags[3]=""; otags[2]=""; otags[1]="${otags[0]}"
      elif [ ! "${otags[3]}" == "" ]; then
        echo -e "    </${otags[3]}>\n  </${otags[2]}>" >> $tmp
        echo -e "</${otags[1]}>\n$line" >> $tmp
        otags[3]=""; otags[2]=""; otags[1]="${otags[0]}"
      elif [ ! "${otags[2]}" == "" ]; then
        echo -e "  </${otags[2]}>\n</${otags[1]}>\n$line" >> $tmp
        otags[2]=""; otags[1]="${otags[0]}"
      elif [ ! "${otags[1]}" == "" ]; then
        echo -e "</${otags[1]}>\n$line" >> $tmp
        otags[1]="${otags[0]}"
      else
        otags[1]="${otags[0]}"
        inner_html=$(echo "$line" | cut -f2 -d">")
        full_tag="<"$(echo "$line" | cut -f2 -d"<" | cut -f1 -d">")">"
        if [[ "$full_tag" =~ "$line" ]]; then
          echo -e "$full_tag$inner_html" >> $tmp
        else
          # If text is on the same line as the tag, close the tag
          # at the end of the line
          closed_line="$full_tag$inner_html</${otags[1]}>"
          echo -e "$closed_line" >> $tmp
          otags[1]=""
        fi
      fi
      ;;
    *) # No tag
      # Empty line: skip
      if [ "$line" == "" ]; then
        continue
      # Outdented line with an open tag at the same level: close the tag
      else
        # Count the leading spaces in the current and previous lines
        cr_indent=$(echo "$line" | awk -F'[^ ]' '{print length($1)}')
        pv_indent=$(echo "$pv_line" | awk -F'[^ ]' '{print length($1)}')
        is_deindent=$(echo "$cr_indent < $pv_indent" | bc)
        if [[ "$is_deindent" == 1 ]]; then
          # Find the open tag and close it
          if [ ! "${otags[6]}" == "" ]; then
            echo -e "          </${otags[6]}>\n$line" >> $tmp
            otags[6]=""
          elif [ ! "${otags[5]}" == "" ]; then
            echo -e "        </${otags[5]}>\n$line" >> $tmp
            otags[5]=""
          elif [ ! "${otags[4]}" == "" ]; then
            echo -e "      </${otags[4]}>\n$line" >> $tmp
            otags[4]=""
          elif [ ! "${otags[3]}" == "" ]; then
            echo -e "    </${otags[3]}>\n$line" >> $tmp
            otags[3]=""
          elif [ ! "${otags[2]}" == "" ]; then
            echo -e "  </${otags[2]}>\n$line" >> $tmp
            otags[2]=""
          elif [ ! "${otags[1]}" == "" ]; then
            echo -e "</${otags[1]}>\n$line" >> $tmp
            otags[1]=""
          fi
        # Indented line
        else
          echo -e "$line" >> $tmp
        fi
      fi
      ;;
  esac
  pv_line="$line"
done < $input
IFS="$OIFS"

# Close the last open tags
if [ ! "${otags[6]}" == "" ]; then
  echo -e "          </${otags[6]}>\n        </${otags[5]}>" >> $tmp
  echo -e "      </${otags[4]}>\n    </${otags[3]}>" >> $tmp
  echo -e "  </${otags[2]}>\n</${otags[1]}>" >> $tmp
elif [ ! "${otags[5]}" == "" ]; then
  echo -e "        </${otags[5]}>" >> $tmp
  echo -e "      </${otags[4]}>\n    </${otags[3]}>" >> $tmp
  echo -e "  </${otags[2]}>\n</${otags[1]}>" >> $tmp
elif [ ! "${otags[4]}" == "" ]; then
  echo -e "      </${otags[4]}>\n    </${otags[3]}>" >> $tmp
  echo -e "  </${otags[2]}>\n</${otags[1]}>" >> $tmp
elif [ ! "${otags[3]}" == "" ]; then
  echo -e "    </${otags[3]}>\n  </${otags[2]}>" >> $tmp
  echo -e "</${otags[1]}>" >> $tmp
elif [ ! "${otags[2]}" == "" ]; then
  echo -e "  </${otags[2]}>\n</${otags[1]}>" >> $tmp
elif [ ! "${otags[1]}" == "" ]; then
  echo -e "</${otags[1]}>" >> $tmp
fi
otags[6]=""; otags[5]=""; otags[4]=""; otags[3]=""; otags[2]=""; otags[1]=""


echo "Converting tag attributes ..."
cp -r $tmp $output
while read line; do
  # Remove escape character "#" from indented inner html
  case "$pv_line" in
    *\#)
      inner_html=$(echo "$pv_line" | cut -f2 -d">")
      clean_inner=$(echo "$inner_html" | sed "s|#||")
      sed -i "s|$inner_html|$clean_inner|" $tmp
  esac
  case "$line" in
    *\<*)
      tag_attrs=$(echo "$line" | cut -f2 -d"<" | cut -f1 -d">")
      # Filter for only classes and ids
      if [[ "$tag_attrs" =~ " ." ]] || [[ "$tag_attrs" =~ " #" ]]; then
          tag_type=$(echo "$line" | cut -f2 -d"<" | cut -f1 -d" " | \
            cut -f1 -d">")
          attr_line=$(echo "$line" | sed "s| \.| class=\"|")
          attr_line=$(echo "$attr_line" | sed "s| \.| |g")
          attr_line=$(echo "$attr_line" | sed "s| \#| id=\"|")
          attr_line=$(echo "$attr_line" | sed "s| class|\" class|")
          attr_line=$(echo "$attr_line" | sed "s| id|\" id|")
          attr_line=$(echo "$attr_line" | sed "s|>|\">|")
          attr_line=$(echo "$attr_line" | sed "s|$tag_type\"|$tag_type|")
          sed -i "s|$line|$attr_line|" $tmp
      fi
      ;;
  esac
  pv_line="$line"
done < $output


echo "Cleaning up html5-invalid tags ..."
sed -i "s|</br>||g" $tmp
sed -i "s|</hr>||g" $tmp
sed -i "s|<img>||g" $tmp
sed -i "s|</img>||g" $tmp
sed -i '/^[[:space:]]*$/d' $tmp

mv $tmp $output
