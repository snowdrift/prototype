# README

**The scripts in this directory are horribly unoptimised and may have side
effects including timesinks, headaches, elevated blood pressure levels and
beverage implosions. Use them at your own risk.**

- `export-svg.sh` — exports a folder of .svg files to .png. Requires inkscape
  and pngquant. See script for usage.

- `lazyham.sh` — partially-implemented conversion of .hamlet files to html.
  Usage: `./util/lazyham.sh page.hamlet`


## License

CC0
