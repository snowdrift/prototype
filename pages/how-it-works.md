---
title: How it Works
---

<main class="main-content --tilt">
  <header class="main-header --tilt">
    <h1 class="__title">How it works</h1>
  </header>

  <section class="intro-images">
    <div class="__img --intro1"></div>
    <div class="__arrow --up"></div>
    <p class="__caption --up">
      Every patron matches every other.<br>
      Projects receive their donations monthly.
    </p>
    <p class="__caption">
      As a patron you set your monthly limit.<br>
      Never will you pay more than that amount.
    </p>
    <div class="__arrow --down"></div>
    <div class="__img --intro2"></div>
  </section>

  <section class="sign-up">
  {% if user.logged_in == false %}
    <a class="__button" href="/auth/login">Sign Up</a>
  {% endif %}
    <a class="__link" href="https://wiki.snowdrift.coop/about">learn more</a>
  </section>
</main>
