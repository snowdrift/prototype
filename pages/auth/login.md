---
title: Log In
---

<main class="main-content">
  <header class="main-header --logo-small">
    <h1 class="__title">Log in to Snowdrift.coop</h1>
  </header>

  <section class="auth-box --white --form">
    <form class="login-form" method="post" enctype="application/x-www-form-urlencoded">
      <div class="labeled-input">
        <input id="hident2" name="f1" class="__field --email" type="email"
               placeholder="email" required autofocus>
        <label class="__label --email" for="hident2">email</label>
      </div>
      <div class="labeled-input">
        <input id="hident3" name="f2" class="__field --passphrase"
               placeholder="passphrase" type="password" required minlength="9">
        <label class="__label --passphrase" for="hident2">passphrase</label>
      </div>
      <p class="__reset"><a class="__link" href="/auth/reset-passphrase">Reset passphrase?</a></p>
      <button class="__button" type="submit">Log in</button>
    </form>
  </section>

  <section class="auth-box --white --oneline">
      Don't have an account? <a class="__link" href="/auth/create-account">
      Sign up!</a>
  </section>
</main>
