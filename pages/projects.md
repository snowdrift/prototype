---
title: Projects
---

<main class="main-content --tilt">
  <header class="main-header --tilt">
    <h1 class="__title">Projects</h1>
  </header>

  <section class="intro-text">
    As soon as possible, more projects will be listed here for support via
    Snowdrift.coop. After all, that's the whole point! For now, pledging to
    support Snowdrift.coop itself will help us get to that point.
  </section>

  <section class="featured-projects">
    <div class="__fader"></div>
    <ol class="projects-list">
      <li class="__item">
        <div class="__project">
          <a class="__link" href="/p/snowdrift">
            <img class="__logo" src="/img/projects/snowdrift-logo.png">
            <div class="__name">Snowdrift.coop</div>
          </a>
        </div>
      </li>
      <li class="__item">
        <div class="__project">
          <div class="__desc">… more coming soon</div>
        </div>
      </li>
      <li class="__item">
        <div class="__project">
          <div class="__desc">… no really, this is not just about us</div>
        </div>
      </li>
    </ul>
  </section>

  {% if user.logged_in == false %}
    <section class="sign-up">
      <div class="__img"></div>
      <a class="__button" href="/auth/login">Sign Up</a>
      <a class="__link" href="/how-it-works">how it works</a>
    </section>
  {% endif %}
</main>
