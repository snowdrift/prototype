---
title: About
---

<main class="main-content --tilt">
  <header class="main-header --tilt">
    <h1 class="__title">About Snowdrift.coop</h1>
  </header>

  <section class="intro-text">
    <p class="__intro">
      Snowdrift.coop is a nonprofit cooperative run by an international team
      driven by a common goal:
    </p>
    <p class="__goal">
      To dramatically improve the ability of ordinary people to fund public
      goods – things like software, music, journalism, and research – that
      everyone can use and share without limitations.
    </p>
  </section>

  <section id="status" class="status-stages">
    <h2 class="__heading">Status</h2>
    <dl class="__stages">
      <div class="__stage">
        <dt class="__status --checked"><span class="__text">Functioning</span></dt>
        <dd class="__desc">
          Initial pledging to the platform itself as the first test project
        </dd>
      </div>
      <div class="__stage">
        <dt class="__status"><span class="__text">Next steps</span></dt>
        <dd class="__desc">
          Run a first real charge for active patrons; clean up site design
        </dd>
      </div>
      <div class="__stage">
        <dt class="__status"><span class="__text">Next major milestone</span></dt>
        <dd class="__desc">
          Enabling the first outside projects for active listing
        </dd>
      </div>
    </dl>
    <a class="__link" href="https://wiki.snowdrift.coop/planning">
      See our roadmap
    </a>
  </section>

  <section class="site-links">
    <p class="__intro">
      More details and updates are available in the following places:
    </p>
    <ul class="__sites">
      <li class="__site">
        <a href="https://wiki.snowdrift.coop">
          <img src="/img/about/wiki.png">
        </a>
        <p class="__desc">
          Read about the entire background of the project and find various
          resources in <a class="__link" href="https://wiki.snowdrift.coop">our
          wiki</a>.
        </p>
      </li>
      <li class="__site">
        <a href="https://community.snowdrift.coop">
          <img src="/img/about/community.png">
        </a>
        <p class="__desc">
          Browse <a class="__link" href="https://community.snowdrift.coop">our
          forum</a> categories to read, comment or post about anything related
          to Snowdrift.coop.
        </p>
      </li>
      <li class="__site">
        <a href="https://gitlab.com/snowdrift/">
          <img src="/img/about/git.png">
        </a>
        <p class="__desc">
          Squash bugs or have a look at how things are evolving in <a
          class="__link" href="https://gitlab.com/snowdrift/">our git
          repository.</a>
        </p>
      </li>
      <li class="__site">
        <a href="https://blog.snowdrift.coop">
          <img src="/img/about/blog.png">
        </a>
        <p class="__desc">
          Read occasional <a class="__link" href="https://blog.snowdrift.coop">
          updates and thoughts</a> about Snowdrift.coop.
        </p>
      </li>
    </ul>
  </section>
</main>
