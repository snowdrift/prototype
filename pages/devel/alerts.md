---
title: Notifications (Alerts)
---

<main class="main-content --tilt">
  <header class="main-header --tilt">
    <h1 class="__title">Notifications (Alerts)</h1>
  </header>

  <section class="devel-info">
    <h2 class="__heading">Navbar</h2>
    <ul class="specs-list">
      <li>Navbar notifications appear under the main navigation menu.</li>
      <li>Users can press the notification title to view the notification
      details, or press the close icon in the upper right corner to dismiss the
      notification.</li>
      <li>Only 1 notification can be expanded at a time. Expanding a
      notifification will collapse the others on the list.</li>
      <li>A maximum of 3 notifications can be stacked under the navigation bar.
      When there are more than three new notifications, a tab should appear at
      the bottom of the list with a link to the notification area in the
      dashboard.</li>
    </ul>
  </section>

  <section class="devel-info">
    <h2 class="__heading">Page</h2>
    <ul class="specs-list">
      <li>Page notifications appear inside the main content container, under
      the header.</li>
      <li>Messages are displayed in full. The notification box has no collapsed
      state.</li>
    </ul>
  </section>

  <!-- Begin page alert -->
  <input class="alert-close --page" type="checkbox">
  <div class="alert-box --page --warning">
    <h2 class="__title">Warning notification</h2>
    <div class="__body">
      <p class="__msg">
        Come on in, I’ve got to tell you what a state I’m in.
      </p>
      <div class="__img --me"></div>
    </div>
  </div>
  <!-- End page alert -->

  <!-- Begin page alert -->
  <input class="alert-close --page" type="checkbox">
  <div class="alert-box --page --warning --img">
    <h2 class="__title">Uh-oh!</h2>
    <div class="__body">
      <p class="__msg">
        Your limit is getting close to your crowdmatch total. This means you
        might have to drop support for a project soon.
      </p>
      <a class="__button" href="/dashboard">review my options</a>
      <div class="__img"></div>
    </div>
  </div>
  <!-- End page alert -->

  <section class="devel-info">
    <h2 class="__heading">Dashboard</h2>
    <ul class="specs-list">
      <li>These notifications appear in the notification area of the user
      dashboard.</li>
      <li>Each notification can be expanded independently of others on the
      page.</li>
    </ul>
  </section>

  <!-- Begin dashboard alert -->
  <input class="alert-close --dashboard" type="checkbox">
  <input class="alert-expand --dashboard" type="radio">
  <div class="alert-box --dashboard --success">
    <h2 class="__title">Success notification</h2>
    <div class="__body">
      <p class="__msg">
        This is a dashboard notification. It doesn't have a button, but at
        least it has a title and a message body. Smile!
      </p>
      <div class="__img --me"></div>
    </div>
  </div>
  <!-- End alert -->

  <!-- Begin dashboard alert -->
  <input class="alert-close --dashboard" type="checkbox">
  <input class="alert-expand --dashboard" type="radio">
  <div class="alert-box --dashboard --info" >
    <h2 class="__title">Info notification</h2>
    <div class="__body">
      <p class="__msg">
        This is another dashboard notification. It has a title and a message
        body.  There is a close button and a link button. It might even have
        a cartoon character. Now imagine said character singing, "Country
        roads ..."
      </p>
      <a class="__button" href="/">take me home</a>
      <div class="__img --me"></div>
    </div>
  </div>
  <!-- End dashboard alert -->

  <!-- Begin dashboard alert -->
  <input class="alert-close --dashboard" type="checkbox">
  <input class="alert-expand --dashboard" type="checkbox">
  <div class="alert-box --dashboard --warning">
    <h2 class="__title">Warning notification</h2>
    <div class="__body">
      <a class="__button" href="/dashboard/settings">open dashboard</a>
      <div class="__img --me"></div>
    </div>
  </div>
  <!-- End dashboard alert -->

  <!-- Begin dashboard alert -->
  <input class="alert-close --dashboard" type="checkbox">
  <input class="alert-expand --dashboard" type="checkbox">
  <div class="alert-box --dashboard --danger">
    <h2 class="__title">Danger notification</h2>
    <div class="__body">
      <p class="__msg">
        Goodness gracious, great balls of fire!
      </p>
      <div class="__img --me"></div>
    </div>
  </div>
  <!-- End dashboard alert -->
</main>
