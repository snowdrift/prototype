---
title: Development Notes
---

<main class="main-content --tilt">
  <header class="main-header --tilt">
    <h1 class="__title">Development Notes</h1>
  </header>

  <section class="devel-info">
    <h2 class="__heading">Reference</h2>
    <ul class="ref-list">
      <li>
        <a class="__link" href="/devel/components">UI components</a>
      </li>
    </ul>
  </section>

  <section class="devel-info">
    <h2 class="__heading">Specification drafts</h2>
    <p class="__intro">The following pages supplement specifications
    drafting and are highly subject to change. <strong class="__warning">Do
    not export to production.</strong></p>
    <ul class="specs-list">
      <li>
        <a class="__link" href="/devel/alerts">
          Notifications
        </a>
      </li>
      <li>
        <a class="__link" href="/dashboard/notifications">
          Dashboard notifications area
        </a>
      </li>
    </ul>
  </section>
</main>
