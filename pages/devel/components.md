---
title: UI Components
---

<main class="main-content --tilt">
  <header class="main-header --tilt">
    <h1 class="__title">UI Components</h1>
  </header>

  <section class="section">
    <p class="__intro">See _page.sass and _patterns.sass for styling.</p>
  </section>

  <section class="section">
    <h2 class="__heading">Typography</h2>
    <p class="__text --title">This is title text</p>
    <p class="__text --heading">This is heading text</p>
    <p class="__text --body">This is body text</p>
    <p class="__text --small">This is small text</p>
    <p class="__text --smallest">This is the smallest text</p>
    <p class="__text --h1">H1 The quick fox jumps over the lazy dog</p>
    <p class="__text --h2">H2 The quick fox jumps over the lazy dog</p>
    <p class="__text --h3">H3 The quick fox jumps over the lazy dog</p>
    <p class="__text --h4">H4 The quick fox jumps over the lazy dog</p>
    <p class="__text --h5">H5 The quick fox jumps over the lazy dog</p>
    <p class="__text --h6">H6 The quick fox jumps over the lazy dog</p>
    <a class="__text --link">This is a link</a>
  </section>

  <section class="section">
    <h2 class="__heading">Horizontal rules</h2>
    <hr class="__hr">
    <hr class="__hr --tilt">
  </section>

  <section class="section">
    <h2 class="__heading">Navigation tab menus</h2>
    <nav class="__tab-nav">
      <a class="__link" href="#">Link</a>
      <a class="__link" href="#">Link</a>
      <a class="__link --active" href="#">Active Link</a>
    </nav>
  </section>

  <section class="section">
    <h2 class="__heading">Buttons</h2>
    <a class="__button --small" href="#">Small Button</a>
    <a class="__button --medium" href="#">Medium Button</a>
    <a class="__button --big" href="#">Big Button</a>
  </section>
</main>
