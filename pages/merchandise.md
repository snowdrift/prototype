---
title: Merchandise
---

<main class="main-content --tilt">
  <header class="main-header --tilt">
    <h1 class="__title">Merchandise</h1>
  </header>

  <section class="merch-section --intro">
    The following gifts are available to those who <a class="__link"
    href="/donate">donate</a> to our launch development outside of
    crowdmatching (or who put in substantial time <a class="__link"
    href="https://wiki.snowdrift.coop/community/how-to-help">volunteering</a>).
  </section>

  <section id="stickers" class="merch-section --stickers">
    <h2 class="__head">Stickers</h2>
    <p class="__intro">
      Our durable stickers will hold up when used outdoors, on any laptop lid
      or anywhere else you want to put them.
    </p>
    <figure class="__item">
      <img class="__img --logo" src="/img/merchandise/sticker-logo.png"
      alt="logo sticker">
      <figcaption class="__desc">»logo« 1.42" x 1.5"</figcaption>
    </figure>
    <figure class="__item">
      <img class="__img --logo-long"
      src="/img/merchandise/sticker-logo-long.png" alt="long logo sticker">
      <figcaption class="__desc">»logo long« 4.61" x 1"</figcaption>
    </figure>
    <figure class="__item">
      <img class="__img --handshake"
      src="/img/merchandise/sticker-handshake.png" alt="shaking hands sticker">
      <figcaption class="__desc">»handshake« 2.5" x 2.06"</figcaption>
    </figure>
    <figure class="__item">
      <img class="__img --shovel" src="/img/merchandise/sticker-shovel.png"
      alt="shovel sticker">
      <figcaption class="__desc">»shovel« 2.5" x 1.77"</figcaption>
    </figure>
    <figure class="__item">
      <img class="__img --hexagon" src="/img/merchandise/sticker-hexagon.png"
      alt="hexagon sticker">
      <figcaption class="__desc">»hexagon« 1.73" x 2"</figcaption>
    </figure>
  </section>

  <section id="t-shirts" class="merch-section --shirts">
    <h2 class="__head">T-Shirts</h2>
    <p class="__intro">
      We use premium fitted shirts made with 100% cotton super-soft jersey
      fabric.
    </p>
    <figure class="__item">
      <a href="/img/merchandise/t-shirt-womens-white_big.jpg"><img
      class="__img" src="/img/merchandise/t-shirt-womens-white_small.jpg"
      alt="t-shirt: women's white"></a>
      <figcaption class="__desc">
        white / women's<br>
        XS, S, M, L, XL, 2XL, 3XL
      </figcaption>
    </figure>
    <figure class="__item">
      <a href="/img/merchandise/t-shirt-womens-blue_big.jpg"><img class="__img"
      src="/img/merchandise/t-shirt-womens-blue_small.jpg" alt="t-shirt:
      women's blue"></a>
      <figcaption class="__desc">
        blue / women's<br>
        XS, S, M, L, XL, 2XL, 3XL
      </figcaption>
    </figure>
    <figure class="__item">
      <a href="/img/merchandise/t-shirt-mens-white_big.jpg"><img class="__img"
      src="/img/merchandise/t-shirt-mens-white_small.jpg" alt="t-shirt: men's
      white"></a>
      <figcaption class="__desc">
        white / men's<br>
        XS, S, M, L, XL, 2XL, 3XL, 4XL
      </figcaption>
    </figure>
    <figure class="__item">
      <a href="/img/merchandise/t-shirt-mens-blue_big.jpg"><img class="__img"
      src="/img/merchandise/t-shirt-mens-blue_small.jpg" alt="t-shirt: men's
      blue"></a>
      <figcaption class="__desc">
        blue / men's<br>
        XS, S, M, L, XL, 2XL, 3XL, 4XL
      </figcaption>
    </figure>
    <figure class="__item">
      <a href="/img/merchandise/t-shirt-womens-heather_big.jpg"><img
      class="__img" src="/img/merchandise/t-shirt-womens-heather_small.jpg"
      alt="t-shirt: women's heather gray"></a>
      <figcaption class="__desc">
        heather gray / women's<br>
        XS, S, M, L, XL, 2XL, 3XL
      </figcaption>
    </figure>
    <figure class="__item">
      <a href="/img/merchandise/t-shirt-womens-warm_big.jpg"><img class="__img"
      src="/img/merchandise/t-shirt-womens-warm_small.jpg" alt="t-shirt:
      women's warm gray"></a>
      <figcaption class="__desc">
        warm gray / women's<br>
        XS, S, M, L, XL, 2XL, 3XL
      </figcaption>
    </figure>
    <figure class="__item">
      <a href="/img/merchandise/t-shirt-mens-heather_big.jpg"><img
      class="__img" src="/img/merchandise/t-shirt-mens-heather_small.jpg"
      alt="t-shirt: men's heather gray"></a>
      <figcaption class="__desc">
        heather gray / men's<br>
        XS, S, M, L, XL, 2XL, 3XL, 4XL
      </figcaption>
    </figure>
    <figure class="__item">
      <a href="/img/merchandise/t-shirt-mens-warm_big.jpg"><img class="__img"
      src="/img/merchandise/t-shirt-mens-warm_small.jpg" alt="t-shirt: men's
      warm gray"></a>
      <figcaption class="__desc">
        warm gray / men's<br>
        XS, S, M, L, XL, 2XL, 3XL, 4XL
      </figcaption>
    </figure>
  </section>
</main>
